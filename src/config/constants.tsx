import { RadioGroupOption } from './types';

export const text = {
  monthsHint: 'Please select the investment time range which is most suitable to your needs. We will adjust your portfolio accordingly.',
};

export const monthsOptions: Array<RadioGroupOption> = [
  {
    value: 3,
    label: 'Short term',
  },
  {
    value: 6,
    label: 'Medium term',
  },
  {
    value: 12,
    label: 'Long term',
  },
  {
    value: 36,
    label: 'Very long term',
  },
];

export const riskOptions: Array<RadioGroupOption> = [
  {
    value: 0,
    label: 'Minimise risk with constant profit',
  },
  {
    value: 1,
    label: 'Maximise profit with current risk level',
  }
];