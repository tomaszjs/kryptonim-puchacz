import React from 'react';
import styled from 'styled-components';
import ControlLabel from './ControlLabel';
import FormControlHint from './FormControlHint';

const StyledFormControl = styled.label`
  display: block;
  line-height: 24px;
  margin-top: 16px;

  &:first-of-type {
    margin-top: 0;
  }
`;

interface FormControlProps {
  children: React.ReactNode;
  label: string;
  hint?: string;
}

const FormControl: React.FC<FormControlProps> = ({
  children,
  hint,
  label,
}) => (
  <StyledFormControl>
    <ControlLabel text={label} />
    {!!hint && <FormControlHint text={hint} />}
    {children}
  </StyledFormControl>
);

export default FormControl;
