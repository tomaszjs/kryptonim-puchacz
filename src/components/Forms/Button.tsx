import React from 'react';
import styled from 'styled-components';

const StyledButton = styled.button`
  font-size: 16px;
  height: 40px;
  line-height: 40px;
  padding: 0 24px;
  border: 1px solid #007099;
  background: #007099;
  color: #fff;
  margin-right: 16px;
  border-radius: 2px;
  text-transform: uppercase;
  font-weight: 500;
  cursor: pointer;

  &:hover {
    opacity: .9;
    cursor: pointer;
  }

  &:disabled {
    opacity: .666;
    cursor: default;
  }

  &.full-width {
    width: 100%;
  }
`;

enum ButtonType {
  submit = 'submit',
  button = 'button',
}

interface ButtonProps {
  children: React.ReactNode;
  type?: ButtonType
};

const Button: React.FC<ButtonProps> = ({ children, type = ButtonType.submit }) => (
  <StyledButton type={type}>
    {children}
  </StyledButton>
)

export default Button;
