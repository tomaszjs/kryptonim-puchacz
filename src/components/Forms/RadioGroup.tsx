import React from 'react';
import styled from 'styled-components';
import Radio from './Radio';
import { RadioGroupOption } from '../../config/types';

const StyledRadioGroup = styled.div`
  padding: 0 4px;

  &.inline {
    display: flex;

    > * {
      margin-top: 0;
      margin-right: 16px;
    }
  }
`;

interface RadioGroupProps {
  controlId: string,
  inline: boolean,
  options: Array<RadioGroupOption>,
  onChange: React.EventHandler<any>,
  value: string | number;
};

const RadioGroup: React.FC<RadioGroupProps> = ({
  controlId,
  inline = false,
  options = [],
  onChange,
  value = '',
}) => {
  const renderOptions = () => options.map((o) => {
    const radioId = `${controlId}-${o.value}`;
    return (
      <Radio
        checked={o.value === value}
        controlId={radioId}
        key={radioId}
        label={o.label}
        onChange={onChange}
        value={o.value}
      />
    );
  })

  return (
    <StyledRadioGroup className={inline ? 'inline' : ''}>
      {renderOptions()}
    </StyledRadioGroup>
  );
}

export default RadioGroup;
