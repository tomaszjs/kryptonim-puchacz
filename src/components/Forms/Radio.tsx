import React from 'react';
import styled from 'styled-components';

const StyledRadio = styled.div`
  font-size: 14px;
  line-height: 20px;
  margin-top: 8px;

  &:first-child {
    margin-top: 0;
  }

  > * {
    display: inline-block;
    vertical-align: middle;
  }

  label {
    cursor: pointer;
    color: #1c1c1c;
    padding: 0 12px;

    &:hover {
      color: #23A2AD;
    }
  }

  input:checked {
    + label {
      color: #262626;
    }
  }
`;

interface RadioProps {
  checked: boolean,
  controlId: string,
  label: string,
  onChange: React.EventHandler<any>,
  value: string | number;
};

const Radio: React.FC<RadioProps> = ({
  checked = false,
  controlId,
  label,
  onChange,
  value = '',
}) => (
  <StyledRadio>
    <input
      checked={checked}
      id={controlId}
      onChange={onChange}
      type="radio"
      value={value}
    />
    <label htmlFor={controlId}>{label}</label>
  </StyledRadio>
);

export default Radio;
