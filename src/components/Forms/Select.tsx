import React from 'react';
import styled from 'styled-components';
import ReactSelect from 'react-select';

const StyledReactSelect = styled(ReactSelect)`
  .Select__control {
    border-radius: 2px;
    border: 1px solid #919191;
    color: #1c1c1c;
    height: 40px;
    font-size: 14px;
    width: 100%;

    &:hover {
      border-color: #676767;
    }

    &.Select__control--is-focused {
      border-color: #009bd2;
      box-shadow: none;


      .Select__indicator,
      .Select__dropdown-indicator {
        color: #009bd2;
      }
    }
  }

  .Select__indicator-separator {
    display: none;
  }

  .Select__indicator,
  .Select__dropdown-indicator {
    color: #676767;

    &:hover {
      color: #676767;
    }
  }

  .Select__value-container {
    font-size: 14px;
    padding: 0 12px;

    > div {
      margin: 0;
    }
  }

  .Select__menu {
    overflow: hidden;
    border-radius: 2px;

    .Select__menu-list {
      padding: 0;

      .Select__option {
        color: #1c1c1c;
        font-size: 14px;
        line-height: 40px;
        padding: 0 12px;

        &:hover {
          cursor: pointer;
        }

        &.Select__option--is-focused {
          background: #f4f4f4;
        }

        &.Select__option--is-selected {
          background: #009bd2;
          color: #fff;
        }
      }
    }
  }
`;

interface SelectOption {
  value: string | number;
  label: string;
};

interface SelectProps {
  options: Array<SelectOption>;
  onChange: Function;
  placeholder?: string;
  value: string;
}

const Select = ({
  options,
  onChange,
  placeholder = '',
  value = '',
}: SelectProps) => {
// const Select: React.FC<SelectProps> = ({
//   controlId,
//   options,
//   onChange,
//   placeholder,
//   value,
// }) => {
  const selectedOption = options.find((o: SelectOption) => o.value === value);

  return (
    <StyledReactSelect
      classNamePrefix="Select"
      options={options}
      onChange={onChange}
      placeholder={placeholder}
      value={selectedOption}
    />
  );
};

export default Select;
