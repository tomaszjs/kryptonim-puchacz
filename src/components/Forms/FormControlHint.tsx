import React from 'react';
import styled from 'styled-components';

const StyledFormControlHint = styled.span`
  display: block;
  line-height: 24px;
  font-size: 12px;
  margin-top: -8px;
  margin-bottom: 8px;
  color: #363636;
`;

interface FormControlHintProps {
  text: string;
};

const FormControlHint: React.FC<FormControlHintProps> = ({ text }) => (
  <StyledFormControlHint>{text}</StyledFormControlHint>
);

export default FormControlHint;
