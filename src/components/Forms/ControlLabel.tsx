import React from 'react';
import styled from 'styled-components';

const StyledControlLabel = styled.label`
  display: block;
  line-height: 32px;
`;

interface ControlLabelProps {
  text: String;
}

const ControlLabel: React.FC<ControlLabelProps> = ({
  text,
}) => (
  <StyledControlLabel>{text}</StyledControlLabel>
);

export default ControlLabel;
