import React from 'react';
import styled from 'styled-components';
import FormControl from '../Forms/FormControl';
import RadioGroup from '../Forms/RadioGroup';
import Button from '../Forms/Button';
import { monthsOptions, riskOptions, text } from '../../config/constants';

const StyledAdviceAdvantageFilter = styled.form`
  border-bottom: 1px solid #e4e4e4;
  margin-bottom: 48px;

  .button-wrap {
    padding: 16px 0 32px;
  }
`;

interface AdviceAdvantageFilterProps {
  onSubmit: Function;
};

interface AdviceAdvantageFilterState {
  formData: {
    months: number;
    risk: number;
  }
};

class AdviceAdvantageFilter extends React.Component<AdviceAdvantageFilterProps, AdviceAdvantageFilterState> {
  constructor(props: AdviceAdvantageFilterProps) {
    super(props);

    this.state = {
      formData: {
        months: 3,
        risk: 0,
      },
    };
  }

  handleChange = (name: string) => (event: React.ChangeEvent<HTMLInputElement>) => {
    const { target: { value } } = event;
    this.setState(({ formData }: AdviceAdvantageFilterState): AdviceAdvantageFilterState => ({
      formData: {
        ...formData,
        [name]: parseInt(value),
      },
    }));
  }

  handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const { onSubmit } = this.props;
    const { formData: { months, risk } } = this.state;

    onSubmit(months, risk);
  }

  render() {
    const {
      formData: { months, risk }
    } = this.state;

    return (
      <StyledAdviceAdvantageFilter onSubmit={this.handleSubmit}>
        <h2>Investment options</h2>
        <FormControl
          label="Investment range"
          hint={text.monthsHint}
        >
          <RadioGroup inline={true} value={months} controlId="time-months" options={monthsOptions} onChange={this.handleChange('months')} />
        </FormControl>
        <FormControl label="Risk profit balance">
          <RadioGroup inline={true} value={risk} controlId="risk-balance" options={riskOptions} onChange={this.handleChange('risk')} />
        </FormControl>
        <div className="button-wrap">
          <Button>Apply</Button>
        </div>
      </StyledAdviceAdvantageFilter>
    );
  }
}

export default AdviceAdvantageFilter;
