import React from 'react';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import { Store } from 'redux';
import { History } from 'history';
import { createGlobalStyle } from 'styled-components';
import { ApplicationState } from '../store';
import AdviceAdvantage from './AdviceAdvantage/AdviceAdventage';

const GlobalStyle = createGlobalStyle`
  * {
    padding: 0;
    margin: 0;
    box-sizing: border-box;
    outline: 0;
    font-family: sans-serif;
  }

  body {
    background: #efefef;
    color: #1c1c1c;
    font-weight: 300;
    overflow-y: scroll;
  }

  html,
  body,
  #root {
    height: 100%;
  }

  a {
    color: #23A2AD;
    text-decoration: none;

    &:hover {
      color: #262626;
    }
  }

  h1, h2, h3 {
    font-weight: 400;
    margin-bottom: 16px;
  }

  p {
    margin: 16px 0;
  }
`;

interface AppProps {
  store: Store<ApplicationState>
  history: History
}

const App: React.FC<AppProps> = ({ store, history }) => {
  return (
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <AdviceAdvantage />
        <GlobalStyle />
      </ConnectedRouter>
    </Provider>
  )
}

export default App;
