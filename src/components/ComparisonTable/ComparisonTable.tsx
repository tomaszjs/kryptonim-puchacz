import React from 'react';
import styled from 'styled-components';

const StyledComparisonTable = styled.table`
  width: 80%;
  border-collapse: collapse;
  line-height: 48px;
  margin: 32px auto;

  .top-th {
    font-weight: 400;
    font-size: 20px;
    width: 40%;
    border-bottom: 1px solid #e4e4e4;
    padding: 0 24px;
  }

  .text-right {
    text-align: right;
  }

  .text-left {
    text-align: left;
  }

  .prop-th {
    width: 20%;
    font-weight: 400;
    background: #f4f4f4;
    // color: #fff;
    border-bottom: 1px solid #e4e4e4;
    text-align: center;
  }

  td {
    padding: 0 24px;
    border-bottom: 1px solid #e4e4e4;
  }

  tbody {
    tr:last-child {
      td {
        border: 0;
      }

      th {
        border: 0;
      }
    }
  }
`;

const ComparisonTable = () => (
  <StyledComparisonTable>
    <thead>
      <tr>
        <th className="top-th text-right">before</th>
        <th className="top-th prop-th">variable</th>
        <th className="top-th text-left">after</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td className="text-right">41cm</td>
        <th className="prop-th">height</th>
        <td className="text-left">92cm</td>
      </tr>
      <tr>
        <td className="text-right">412g</td>
        <th className="prop-th">weight</th>
        <td className="text-left">398g</td>
      </tr>
      <tr>
        <td className="text-right">$9.2k</td>
        <th className="prop-th">total pricing</th>
        <td className="text-left">$8.7k</td>
      </tr>
      <tr>
        <td className="text-right">01/02/19</td>
        <th className="prop-th">end date</th>
        <td className="text-left">01/03/19</td>
      </tr>
      <tr>
        <td className="text-right">dolor sit amet</td>
        <th className="prop-th">lorem ipsum</th>
        <td className="text-left">---</td>
      </tr>
    </tbody>
  </StyledComparisonTable>
);

export default ComparisonTable;
