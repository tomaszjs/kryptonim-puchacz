import React from 'react'
import * as R from 'recharts'

export interface PieChartDataItem {
  name: string
  color: string;
  value: number;
}

export interface PieChartProps {
  width: number
  height: number
  data: PieChartDataItem[]
}

export default class PieChart extends React.Component<PieChartProps, { activeIndex?: number }> {
  state = {
    activeIndex: undefined
  }

  onPieEnter = (_: any, index: number) => {
    this.setState({
      activeIndex: index,
    });
  }

  onPieOut = () => {
    this.setState({
      activeIndex: undefined,
    });
  }

  render() {
    const { width, height, data } = this.props;
    return (
      <R.PieChart {...{ width, height }}>
        <R.Pie
          activeIndex={this.state.activeIndex}
          activeShape={renderActiveShape}
          dataKey='value'
          data={data}
          innerRadius={'50%'}
          outerRadius={'70%'}
          fill="#8884d8"
          onMouseEnter={this.onPieEnter}
          onMouseOut={this.onPieOut}
        >
          {data.map(({ color }, index) => <R.Cell key={`cell-${index}`} fill={color} />)}
        </R.Pie>
      </R.PieChart>
    );
  }
}

interface RenderActiveShapeProps {
  cx: number
  cy: number
  midAngle: number
  innerRadius: number
  outerRadius: number
  startAngle: number
  endAngle: number
  fill: string
  percent: number
  value: number
  payload: PieChartDataItem
}
const renderActiveShape = (props: RenderActiveShapeProps) => {
  const RADIAN = Math.PI / 180;
  const { cx, cy, midAngle, innerRadius, outerRadius, startAngle, endAngle,
    fill, payload: { name }, value } = props;
  const sin = Math.sin(-RADIAN * midAngle);
  const cos = Math.cos(-RADIAN * midAngle);
  const sx = cx + (outerRadius + 10) * cos;
  const sy = cy + (outerRadius + 10) * sin;
  const mx = cx + (outerRadius + 30) * cos;
  const my = cy + (outerRadius + 30) * sin;
  const ex = mx + (cos >= 0 ? 1 : -1) * 22;
  const ey = my;
  const textAnchor = cos >= 0 ? 'start' : 'end';

  return (
    <g>
      <R.Sector
        cx={cx}
        cy={cy}
        innerRadius={innerRadius}
        outerRadius={outerRadius}
        startAngle={startAngle}
        endAngle={endAngle}
        fill={fill}
      />
      <R.Sector
        cx={cx}
        cy={cy}
        startAngle={startAngle}
        endAngle={endAngle}
        innerRadius={outerRadius + 6}
        outerRadius={outerRadius + 10}
        fill={fill}
      />
      <path d={`M${sx},${sy}L${mx},${my}L${ex},${ey}`} stroke={fill} fill="none" />
      <circle cx={ex} cy={ey} r={2} fill={fill} stroke="none" />
      <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} textAnchor={textAnchor} fill="#333">{name}</text>
      <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} dy={18} textAnchor={textAnchor} fill="#999">
        {value}
      </text>
    </g>
  );
};
