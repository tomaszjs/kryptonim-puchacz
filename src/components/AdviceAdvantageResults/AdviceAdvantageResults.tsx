import React from 'react';
import styled from 'styled-components';
import LineChart from '../LineChart/LineChart';
import ComparisonTable from '../ComparisonTable/ComparisonTable';
import TwoPieCharts from '../TwoPieCharts/TwoPieCharts';

const StyledAdviceAdvantageResults = styled.div`
  h2 {
    margin-bottom: 32px;
  }

  .part {
    margin: 32px 0 48px;
  }
`;

const AdviceAdvantageResults = () => (
  <StyledAdviceAdvantageResults>
    <h2>Results</h2>

    <div className="part">
      <h3>Line chart</h3>
      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
      <LineChart />
    </div>

    <div className="part">
      <h3>Comparison table</h3>
      <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
      <ComparisonTable />
    </div>

    <div className="part">
      <h3>Two pie charts</h3>
      <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
      <TwoPieCharts />
    </div>
  </StyledAdviceAdvantageResults>
);

export default AdviceAdvantageResults;

