import React from 'react';
import ReactDOM from 'react-dom';
import { createHashHistory } from 'history';
import configureStore from './configureStore';

import App from './components/App';

const history = createHashHistory();

const initialState = window.initialReduxState;
const store = configureStore(history, initialState);

ReactDOM.render(<App store={store} history={history} />, document.getElementById('root'));
