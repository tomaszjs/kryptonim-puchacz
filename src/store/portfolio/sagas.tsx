import {
  all,
  call,
  fork,
  put,
  takeEvery,
} from 'redux-saga/effects';
import {
  FETCH_PORTFOLIO_REQUESTED,
  FETCH_PORTFOLIO_SUCCESS,
} from './actions';
import { fetchPortfolio } from './api';

function* fetchPortfolioSaga() {
  try {
    const response = yield call(fetchPortfolio());
    yield put({ type: FETCH_PORTFOLIO_SUCCESS, payload: { data: response.data } });
  } catch (err) {
    yield call([console, 'log'], err);
  }
}

function* watchFetchPortfolioSaga() {
  yield takeEvery(FETCH_PORTFOLIO_REQUESTED, fetchPortfolioSaga)
}

function* portfolioSaga() {
  yield all([
    fork(watchFetchPortfolioSaga),
  ]);
}

export default portfolioSaga;
