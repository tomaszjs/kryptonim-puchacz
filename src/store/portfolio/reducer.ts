import { Reducer } from 'redux';
import { PortfolioState } from './types';
import {
  PortfolioActions,
  FETCH_PORTFOLIO_REQUESTED,
  FETCH_PORTFOLIO_SUCCESS,
} from './actions';

const INITIAL_STATE: PortfolioState = {
  loading: false,
  data: null,
};

export const portfolioReducer: Reducer<PortfolioState, PortfolioActions> = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FETCH_PORTFOLIO_REQUESTED:
      return { ...state, loading: true, data: null };

    case FETCH_PORTFOLIO_SUCCESS:
      return { ...state, loading: false, data: action.payload.data };

    default:
      return state;
  }
}